package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"

	satsearch "gitlab.com/riscognition/sat-search"
)

const url = "https://earth-search.aws.element84.com/v0/search"

func main() {
	pm := map[string]interface{}{
		// "collection": "sentinel-s2-l2a-cogs",
		"limit": 20,
	}
	r, err := satsearch.Search(url, pm)
	if err != nil {
		log.Fatal(err)
	}
	write(r)
}

func write(data interface{}) {
	json, err := json.Marshal(data)
	if err != nil {
		fmt.Println(err)
	}
	err = ioutil.WriteFile("data.json", json, 0)
	if err != nil {
		fmt.Println(err)
	}
}
